export default {
    app: {
        token: "cbfe1da9b59e4d97bbe3f2e7e45d14de", // <- enter your token here
        muted: false, // <- mute microphone by default
        watermark: true // <- show watermark
    },
    locale: {
        strings: {
            welcomeTitle: "Hello, ask something to get started",

            offlineTitle: "Oh, no!",
            offlineDescription: "It looks like you are not connected to the internet, this webpage requires internet connection, to process your requests",
            queryTitle: "Don't be shy ask me something...",
            voiceTitle: "Go ahead, im listening..."
        },
        settings: {
            speechLang: "en-GB", // <- output language
            recognitionLang: "en-US" // <- input(recognition) language
        }
    }
}